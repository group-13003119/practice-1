public class Student {
  private String name;
  private String surname = "none";
  private int age;

  public static  int test=0;

  public static int count = 0;

  public Student() {
    this("undefined", "undefined");

    count++;
  }


  public Student(String name, String surname) {
    super();
    this.setName(name);
    this.surname = surname;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }


}
