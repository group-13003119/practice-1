package ExamTask;

import java.util.Scanner;

public class Config {

  public static Student[] students;

  public static String getStringValue(String property) {
    Scanner sc = new Scanner(System.in);
    System.out.print(property);
    String value = sc.nextLine();
    return value;
  }

  public static Integer getIntegerValue(String property) {
    Scanner sc = new Scanner(System.in);
    System.out.print(property);
    Integer value = sc.nextInt();
    return value;
  }
}
