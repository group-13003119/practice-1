package ExamTask;

public class Student {
  private String name;
  private String surname;
  private String group;
  private int score;


  public Student(String name, String surname, String group, int score) {
    this.name = name;
    this.surname = surname;
    this.group = group;
    this.score = score;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getGroup() {
    return group;
  }

  public void setGroup(String group) {
    this.group = group;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }
}
