package ExamTask;

import static ExamTask.ExamUtil.*;

public class Main {

  public static void main(String[] args) {
    System.out.println("Welcome to Student registration process");
    int count = Config.getIntegerValue("Student sayini daxil edin: ");
    Config.students = new Student[count];
    fillStudents(count);
    printAllStudentsWithScore(Config.students);
  }

  public static void fillStudents(int count) {
    for (int i = 0; i < count; i++) {
      System.out.println(i + 1 + ".Yeni studenti daxil edin.");
      Student newStudent = createStudent();
      Config.students[i] = newStudent;
    }
  }

  public static Student createStudent() {
    String name = Config.getStringValue("Name: ");
    String surname = Config.getStringValue("Surname: ");
    String group = Config.getStringValue("Group: ");
    int score = Config.getIntegerValue("Score: ");

    Student newStudent = new Student(name, surname, group, score);
    return newStudent;
  }
}
