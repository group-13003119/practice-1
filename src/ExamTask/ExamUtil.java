package ExamTask;

public class ExamUtil {
  public static void printAllStudentsWithScore(Student[] students) {
    for (int i = 0; i < students.length; i++) {
      Student student = students[i];
      if (ExamUtil.isScoreA(student.getScore())) {
        System.out.println("Name: " + student.getName() + " Result: " + "A");
      } else if (ExamUtil.isScoreB(student.getScore())) {
        System.out.println("Name: " + student.getName() + " Result: " + "B");
      } else if (ExamUtil.isScoreC(student.getScore())) {
        System.out.println("Name: " + student.getName() + " Result: " + "C");
      } else {
        System.out.println("Name: " + student.getName() + " Result: " + "Game OVER");
      }
    }
  }

  public static boolean isScoreA(int score) {
    if (score >= ExamConstants.MIN_VALUE_OF_A
            && score <= ExamConstants.MAX_VALUE_OF_A) {
      return true;

    }
    return false;
  }

  public static boolean isScoreB(int score) {
    if (score >= ExamConstants.MIN_VALUE_OF_B
            && score <= ExamConstants.MAX_VALUE_OF_B) {
      return true;

    }
    return false;
  }

  public static boolean isScoreC(int score) {
    if (score >= ExamConstants.MIN_VALUE_OF_C
            && score <= ExamConstants.MAX_VALUE_OF_C) {
      return true;

    }
    return false;
  }
}
