package ExamTask;

public class ExamConstants {

  public static final int MIN_VALUE_OF_A = 91;
  public static final int MAX_VALUE_OF_A = 100;

  public static final int MIN_VALUE_OF_B = 81;
  public static final int MAX_VALUE_OF_B = 90;

  public static final int MIN_VALUE_OF_C = 71;
  public static final int MAX_VALUE_OF_C = 80;

}
