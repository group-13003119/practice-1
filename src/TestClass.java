import ders.Lesson;

public class TestClass {


  public TestClass() {
    System.out.println("I'm a instance block");
  }

  {
    System.out.println("I'm a non-static  block");
  }


  public static void main(String[] args) {
    Main main = new Main();

    String s = "Salam";///111
    String s1 = "Salam";///111

    String s2 = new String("Salam"); ///333
    String s3 = new String("Salam");///444

    System.out.println(s == s1);
    System.out.println(s == s2);

    System.out.println(s2 == s3);
    System.out.println(s1 == s3);
  }
}
